package com.cch.stock.report.web;

import java.text.ParseException;
import java.util.Map;

import javax.servlet.ServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.cch.platform.web.WebUtil;
import com.cch.stock.report.service.MoneyReportService;

@Controller
@RequestMapping(value = "/report/moneyreport")
public class MoneyReport {

	@Autowired
	private MoneyReportService service;
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/query.do")
	public String  pageQuery(ServletRequest request,ModelMap mm) throws ParseException {
		Map<String, Object> param=WebUtil.getParam(request);
		mm.putAll(service.getData(param));
		return "jsonView";
	}

}
