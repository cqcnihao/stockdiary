<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	
	<jsp:include page="/core/tool/resources.jsp"></jsp:include>
	<script src="./trade.js"></script>
</head>

<body>
	<div id="toolbar">
	   <a class="easyui-linkbutton" iconCls="icon-add" onclick="click_add()">新增</a>
       <a class="easyui-linkbutton" iconCls="icon-remove" onclick="javascript:$('#dg').edatagrid('destroyRow')">删除</a>
<!-- <a class="easyui-linkbutton" iconCls="icon-undo" onclick="javascript:$('#dg').edatagrid('cancelRow')">取消</a>  -->      
	</div>
	<table id="dg" title="交易管理" toolbar="#toolbar"
            data-options="rownumbers:true,singleSelect:true,pagination:true,method:'post',idField:'billId',
            	sortOrder:'desc', sortName:'billDate', editing:false,fit:true,singleSelect:false, pageSize:20,
            	url : plat.fullUrl('/bill/stocktrade/pagequery.do'),
				destroyUrl : plat.fullUrl('/bill/stocktrade/delete.do')
            ">
        <thead>
            <tr>
            	<th data-options="field:'checked',checkbox:true"/>
           	 	<th data-options="field:'billId'" hidden='true'>主键</th>
           	 	<th data-options="field:'billDate',width:100,order:'desc',editor:'datebox'" sortable="true">交易时间</th>
                <th data-options="field:'billCatory',width:80,flexset:'SD_STOCK_TRADE',editor:'combobox'">类别</th>
                <th data-options="field:'stockCode',width:80">代码</th>
                <th data-options="field:'stockName',width:90">名称</th>
                <th data-options="field:'billMoney',width:100,align:'right',editor:'numberbox',precision:2" sortable="true">交易金额</th>
                <th data-options="field:'stockCnt',width:80,align:'right',editor:'numberbox'">数量</th>
                <th data-options="field:'stockPrice',width:80,align:'right',editor:'numberbox',precision:2">价格</th>
                <th data-options="field:'feeCommision',width:80,align:'right',editor:'numberbox',precision:2">佣金</th>
                <th data-options="field:'feeTax',width:80,align:'right',editor:'numberbox',precision:2">印花</th>
                <th data-options="field:'feeOther',width:80,align:'right',editor:'numberbox',precision:2">过户</th>
                <th data-options="field:'feeAll',width:80,align:'right',editor:'numberbox',precision:2">费用合计</th>
                <th data-options="field:'billDesc',width:300">说明</th>
            </tr>
        </thead>
    </table>
    
    <div id="dlg" class="easyui-dialog" style="padding:10px 60px 20px 60px"
    	data-options="title:'交易管理',modal:true,toolbar:'#dlg-toolbar'">
			<form id="ff" method="post" ajax="true">
				<input name="billId" type="hidden"/>
				<table cellpadding="5">
					<tr>
						<td>类别:</td>
						<td><input class="easyui-combobox" name="billCatory" 
							data-options="valueField:'code',textField:'name',data:plat.FlexUtil.getValue('SD_STOCK_TRADE'),required:true,onSelect:dlg_auto_calfee"/></td>
						<td>股票:</td>
						<td><input class="easyui-combobox" name="stockCode"
							data-options="valueField:'code',textField:'name',data:plat.FlexUtil.getValue('SD_STOCK_SEL'),required:true,onSelect:dlg_auto_calfee"/></td>
					</tr>
					<tr>
						<td>交易时间:</td>
						<td><input class="easyui-datetimebox" name="billDate"/></td>
						<td>交易金额:</td>
						<td><input class="easyui-numberbox" name="billMoney" data-options="editable:false,precision:2"/></td>
					</tr>
					<tr>
						<td>数量:</td>
						<td><input class="easyui-numberbox" name="stockCnt" data-options="onChange:dlg_auto_calfee"/></td>
						<td>单价:</td>
						<td><input class="easyui-numberbox" name="stockPrice" data-options="precision:4,onChange:dlg_auto_calfee"/></td>
					</tr>
					<tr>
						<td>佣金:</td>
						<td><input class="easyui-numberbox" name="feeCommision" data-options="precision:4"/></td>
						<td>印花税:</td>
						<td><input class="easyui-numberbox" name="feeTax" data-options="precision:4"/></td>
					</tr>
					<tr>
						<td>过户费:</td>
						<td><input class="easyui-numberbox" name="feeOther" data-options="precision:4"/></td>
						<td>费用合计:</td>
						<td><input class="easyui-numberbox" name="feeAll" data-options="editable:false,precision:4"/></td>
					</tr>
					<tr>
						<td>说明:</td>
						<td colspan="3" rowspan="2"><input class="easyui-textbox" name="billDesc" data-options="width:390"/></td>
					</tr>
				</table>
			</form>
		</div>
	</div>

	<div id="dlg-toolbar">
	   <a class="easyui-linkbutton" iconCls='icon-save' onclick="dlg_click_save()">保存</a>
       <a class="easyui-linkbutton" iconCls="icon-remove" onclick="dlg_click_delete()">删除</a>
       <a class="easyui-linkbutton" iconCls="icon-undo" onclick="dlg_click_cancle()">取消</a>
	</div>

</body>
</html>
