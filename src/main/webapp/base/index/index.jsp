<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
	String path = request.getContextPath();
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>Stock Diary</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	
	<jsp:include page="/core/tool/resources.jsp"></jsp:include>
	<script src="./index.js"></script>
	<link href="./index.css" rel="stylesheet" />
	
	<script type="text/javascript">
		var indexData={contextPath:"<%=path%>"};
		plat.Data.loadData(indexData);
	</script>
</head>

<body class="easyui-layout" style="text-align:left">

	<div id="index_top" data-options="region:'north',border:false">
		<table style="width:100%;">
			<tr>
				<td class="logo"><img src="stock_logo.jpg" alt=""/></td>
				<td>我的股票交易记录 stock diray</td>
				<td class="index_buttons">
					主题:<input id="themes" class="easyui-combobox" 
    					data-options="valueField:'code',textField:'name',data:plat.FlexUtil.getValue('PLAT_THEME'),
    						onSelect:changeTheme
    					"/>
					<a class="easyui-linkbutton"  onclick="logout()">
						退出</a>
				</td>
			</tr>
		</table>
	</div>
	<div data-options="region:'west',split:true" title="菜单"
		style="width:250px;">
		<ul id="index_menu" class="easyui-tree" data-options="animate:true">
		</ul>
	</div>

	<div data-options="region:'center',border:false">
		<div id="index_tabs" class="easyui-tabs" fit="true" border="false" plain="true">
			<div title="首页">股市有风险，交易需谨慎</div>
		</div>
	</div>

<!-- <div data-options="region:'south',split:true" style="height:30px;">
	</div> -->	

</body>
</HTML>


